module cleanprof2callgrind

import StdEnv, StdFunc
import Data.Func, Data.List, Text
import ShowProfile

perMod [] = id
perMod [m:ms] = seq (map perFun [m:ms]) o fwrites ("fl= " +++ m.module_name +++ "\n")

perFun p f = f
	<<< "fn=" <<< funname <<< "\n"
	<<< lineno <<< " "
	<<< toInt p.time <<< " "
	<<< p.n_strict_calls <<< " "
	<<< p.n_lazy_calls <<< " "
	<<< p.n_curried_calls <<< " "
	<<< p.n_allocated_words <<< "\n"
where
	(lineno, funname) = case lastIndexOf ";" p.function_name of
		-1 = (0, p.function_name)
		i = (toInt (dropChars (i+1) p.function_name), subString 0 i p.function_name)

Start :: *World -> *World
Start w
# (io, w) = stdio w
# (profs, io) = open_profile io
# io = seq (map perMod $ groupBy ((==) `on` (\r->r.module_name)) profs) $
	io
		<<< "event: Scalls : Strict calls\n"
		<<< "event: Lcalls : Lazy calls\n"
		<<< "event: Ccalls : Curried calls\n"
		<<< "event: Words : Allocated words\n"
		<<< "events: Time Scalls Lcalls Ccalls Words\n"
= snd (fclose io w)
