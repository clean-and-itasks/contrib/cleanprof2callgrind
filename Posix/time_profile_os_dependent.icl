implementation module time_profile_os_dependent

import StdEnv

clock_speed_and_profile_overhead :: (!Int,!Real,!Real);
clock_speed_and_profile_overhead = (0, 0.0, 0.0);

get_compute_time_function :: !*File -> (!(Int,Int,Int) -> Real,!*File)
get_compute_time_function file = (\(lo,hi,calls)->toReal hi, file)
