implementation module ShowProfile

import StdEnv
import time_profile_os_dependent

open_profile :: !*File -> *(([.Profile]),!*File);
open_profile file
	# (compute_time_function,file) = get_compute_time_function file
	= read_function_profiles compute_time_function file
where
	read_function_profiles :: (.(Int,Int,Int) -> .Real) *File -> ([.Profile],.File);
	read_function_profiles compute_time_function file
		# (ok,function_profile,file) = read_function_profile file
		| not ok
			= ([],file)
			# (profile,file) = read_function_profiles compute_time_function file
			= ([function_profile : profile],file)
	where
		read_function_profile file
			# (ok,module_name,file) = read_function_name file
			| not ok
				= error file
			# (ok,function_name,file) = read_function_name file
			| not ok
				= error file
			# (ok,n_strict_calls,file)=freadi file
			| not ok
				= error file
			# (ok,n_lazy_calls,file)=freadi file
			| not ok
				= error file
			# (ok,n_curried_calls,file)=freadi file
			| not ok
				= error file
			# (ok,n_profiler_calls,file)=freadi file
			| not ok
				= error file
			# (ok,n_allocated_words,file)=freadi file
			| not ok
				= error file
			# (ok,time_hi,file)=freadi file
			| not ok
				= error file
			# (ok,time_lo,file)=freadi file
			| not ok
				# time_lo=time_hi
				# time_hi=0
				# time = compute_time_function (time_hi,time_lo,n_profiler_calls)
				=	(True,
					{ module_name		= module_name
					, function_name		= function_name
					, n_strict_calls	= n_strict_calls
					, n_lazy_calls		= n_lazy_calls
					, n_curried_calls	= n_curried_calls
					, n_allocated_words	= n_allocated_words
					, time				= time
					},file)
			# (ok,c,file) = freadc file
			| not ok || c<>'\n'
				= error file
				# time = compute_time_function (time_hi,time_lo,n_profiler_calls)
				=	(True,
					{ module_name		= module_name
					, function_name		= function_name
					, n_strict_calls	= n_strict_calls
					, n_lazy_calls		= n_lazy_calls
					, n_curried_calls	= n_curried_calls
					, n_allocated_words	= n_allocated_words
					, time				= time
					},file)
		where
				error file = (False,abort "error in read_function_profile",file)
		
		read_function_name :: !*File -> (!Bool,!String,!*File)
		read_function_name file
			# (ok,c,file) = freadc file
			| not ok || c==' ' || c=='\n'
				= (False,"",file)
				# (acc,file) = read_function_name [c] file
				= (True,{c \\ c <- reverse acc},file)
		where
			read_function_name acc file
				# (ok,c,file) = freadc file
				| not ok || c == ' ' || c == '\n' = (acc,file)
				= read_function_name [c:acc] file
