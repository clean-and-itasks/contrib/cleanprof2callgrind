### cleanprof2callgrind
Converts a clean time profile to callgrind format.

### How to build (on POSIX)
```
clm -IL Platform -I Posix cleanprof2callgrind -o cleanprof2callgrind
```

### How to use
```
./cleanprof2callgrind < some_executable\ Time\ Profile.pcl > callgrind.out.1234
```

### Download
Linux 64 bit builds can be downloaded [here](https://gitlab.science.ru.nl/clean-and-itasks/cleanprof2callgrind/builds/artifacts/master/browse?job=test)
