definition module ShowProfile

from StdFile import class FileSystem

:: Profile =
	{ module_name		:: String
	, function_name		:: String
	, n_strict_calls	:: Int
	, n_lazy_calls		:: Int
	, n_curried_calls	:: Int
	, n_allocated_words	:: Int
	, time				:: Real
	}

open_profile :: !*File -> *(([.Profile]),!*File);
